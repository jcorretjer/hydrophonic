# Ver. 0.1.2

from time import sleep
import datetime
import busio
import digitalio
import board
import adafruit_mcp3xxx.mcp3008 as MCP
from adafruit_mcp3xxx.analog_in import AnalogIn
import pyrebase
from pyfcm import FCMNotification


def convertVoltsToPh(volts):
    return (-5.6548 * volts) + 15.509

def DebuggingBuzzer(sleepAmount):
    """Buzzer used for logging / debugging"""
    # BZ.on()
    #
    # sleep(sleepAmount)
    #
    # BZ.off()
    #
    # sleep(sleepAmount)


if __name__ == "__main__":

    # region Inits

    # Keeps track of the previous flag state. That way we only send one firebase update per sensor read
    overThreshold = False

    PH_NEUTRAL = 7.0

    ACID_THRESHOLD = 5.0

    ALKALINE_THRESHOLD = 9.0

    ANALOG = AnalogIn(
        MCP.MCP3008(busio.SPI(clock=board.SCK, MISO=board.MISO, MOSI=board.MOSI), digitalio.DigitalInOut(board.D5)), MCP.P1)

    # region Firebase.

    # Any questions about the api check this basic guide https://github.com/thisbejim/Pyrebase

    # To ge this info,  Go into project settings. Add a web app version down at teh bottom
    FIREBASE = pyrebase.initialize_app({
        "apiKey": "AIzaSyAr8wwKXq2uVFIZ8sSaw89u7Q0jgvgpJRo",
        "authDomain": "hydrophonic-6d4d1.firebaseapp.com",
        "databaseURL": "https://hydrophonic-6d4d1-default-rtdb.firebaseio.com",
        "projectId": "hydrophonic-6d4d1",
        "storageBucket": "hydrophonic-6d4d1.appspot.com",
        "messagingSenderId": "551352970789",
        "appId": "1:551352970789:web:4db9e30ff5193750e1d9cc",
        "measurementId": "G-90DHJTSQBT"
    })

    FIREBASE_AUTH = FIREBASE.auth()

    # Wait 15 secs for the pi to finish up booting before doing real work
    #sleep(15)

    # region Since this script runs on boot up, this lets me know it ran

    # BZ = Buzzer(20)
    #
    DebuggingBuzzer(1)

    # endregion

    firebaseUser = None

    # Sometimes it randomly fails to retrieve the user account.
    try:
        firebaseUser = FIREBASE_AUTH.sign_in_with_email_and_password("opensource_corretjer@hotmail.com", "chelo666^^^")

    except Exception as e:

        print(e)

        # Loop buzzer because there was a connection error and I should reboot the PI
        while True:
            DebuggingBuzzer(1)

    # region Alert it's running without any user retrieval problems

    DebuggingBuzzer(1)

    DebuggingBuzzer(1)

    # endregion

    FIREBASE_DB = FIREBASE.database()

    PUSH_SERVICE = FCMNotification(
        api_key="AAAAgF8yriU:APA91bEJhahTXsPmxjoAynGOM0ZPO2-eJAOyJl6MvJxXR_-FolM8r0xFRCELapRruQBtNywG9Pp74vy74eoy9OXjyMoRBTOdiYWan9pndvT0v4XWeLhzcIu2pzoTkonBZO05zlon8dIi ")

    AUTH_TIMEOUT_TRIGGER = 1800
    """After an hour(30 mins = 1800 secs), the auth token expires."""

    authLoginDate = datetime.datetime.now()

    # endregion

    # endregion

    PH_FIRE_BASE_DB_KEY = "ph"

    while True:

        ph = convertVoltsToPh(ANALOG.voltage)

        FIREBASE_DB.update({PH_FIRE_BASE_DB_KEY: ph}, firebaseUser['idToken'])

        print("pH =", ph)

        # Refresh login token because, by design, it expires every hour.
        if (datetime.datetime.now() - authLoginDate).total_seconds() >= AUTH_TIMEOUT_TRIGGER:
            firebaseUser = FIREBASE_AUTH.refresh(firebaseUser['refreshToken'])

            authLoginDate = datetime.datetime.now()

        if ph >= ALKALINE_THRESHOLD:

            overThreshold = True

            print("pH too high!")

        elif ph <= ACID_THRESHOLD:

            overThreshold = True

            print("pH too low!")

        else:

            overThreshold = False

        sleep(2)
