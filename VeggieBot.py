# Ver. 4.0.0

from time import sleep
import datetime
import busio
import digitalio
import board
import adafruit_mcp3xxx.mcp3008 as MCP
from adafruit_mcp3xxx.analog_in import AnalogIn
import pyrebase
from pyfcm import FCMNotification

import enum


class PhStates(enum.Enum):
    ACID = 1

    BASE = 2

    NEUTRAL = 3


def convertVoltsToPh(volts):
    return (-5.6548 * volts) + 15.509


def DebuggingBuzzer(sleepAmount):
    """Buzzer used for logging / debugging"""


def calcAcidThreshold(threshold, neutral):
    return neutral - threshold


def calcBaseThreshold(threshold, neutral):
    return neutral + threshold

    # BZ.on()
    #
    # sleep(sleepAmount)
    #
    # BZ.off()
    #
    # sleep(sleepAmount)


if __name__ == "__main__":

    # region Inits

    SUBSCRIBER_TOPIC = "PH"

    BASE_HIGH_TITLE = "Ph levels too high!"

    BASE_HIGH_BODY = "Add more base(Ph up). Tap this notification to view PH data"

    ACID_HIGH_TITLE = "Ph levels too low!"

    ACID_HIGH_BODY = "Add more acid(Ph down). Tap this notification to view PH data"

    PH_NEUTRAL_TITLE = "Ph levels stablelized"

    PH_NEUTRAL_BODY = "Tap this notification to view PH data"

    # Keeps track of the current ph state. That way we only send one push notification per sensor read
    phState = PhStates.NEUTRAL

    userPhNeutral = 6.0

    threshold = 0.5

    acidThreshold = userPhNeutral - threshold

    baseThreshold = userPhNeutral + threshold

    ANALOG = AnalogIn(
        MCP.MCP3008(busio.SPI(clock=board.SCK, MISO=board.MISO, MOSI=board.MOSI), digitalio.DigitalInOut(board.D5)), MCP.P1)

    # region Firebase.

    # Any questions about the api check this basic guide https://github.com/thisbejim/Pyrebase

    # To ge this info,  Go into project settings. Add a web app version down at the bottom
    FIREBASE = pyrebase.initialize_app({
        "apiKey": "AIzaSyAr8wwKXq2uVFIZ8sSaw89u7Q0jgvgpJRo",
        "authDomain": "hydrophonic-6d4d1.firebaseapp.com",
        "databaseURL": "https://hydrophonic-6d4d1-default-rtdb.firebaseio.com",
        "projectId": "hydrophonic-6d4d1",
        "storageBucket": "hydrophonic-6d4d1.appspot.com",
        "messagingSenderId": "551352970789",
        "appId": "1:551352970789:web:4db9e30ff5193750e1d9cc",
        "measurementId": "G-90DHJTSQBT"
    })

    FIREBASE_AUTH = FIREBASE.auth()

    # Wait 15 secs for the pi to finish up booting before doing real work
    # sleep(15)

    # region Since this script runs on boot up, this lets me know it ran

    # BZ = Buzzer(20)
    #
    DebuggingBuzzer(1)

    # endregion

    firebaseUser = None

    # Sometimes it randomly fails to retrieve the user account.
    try:
        firebaseUser = FIREBASE_AUTH.sign_in_with_email_and_password("opensource_corretjer@hotmail.com", "chelo666^^^")

    except Exception as e:

        print(e)

        # Loop buzzer because there was a connection error and I should reboot the PI
        while True:
            DebuggingBuzzer(1)

    # region Alert it's running without any user retrieval problems

    DebuggingBuzzer(1)

    DebuggingBuzzer(1)

    # endregion

    FIREBASE_DB = FIREBASE.database()

    PUSH_SERVICE = FCMNotification(
        api_key="AAAAgF8yriU:APA91bEJhahTXsPmxjoAynGOM0ZPO2-eJAOyJl6MvJxXR_-FolM8r0xFRCELapRruQBtNywG9Pp74vy74eoy9OXjyMoRBTOdiYWan9pndvT0v4XWeLhzcIu2pzoTkonBZO05zlon8dIi ")

    AUTH_TIMEOUT_TRIGGER = 1800
    """After an hour(30 mins = 1800 secs), the auth token expires."""

    authLoginDate = datetime.datetime.now()

    # endregion

    # endregion

    PH_FIRE_BASE_DB_KEY = "ph"

    THRESHOLD_FIRE_BASE_DB_KEY = "threshold"

    USER_NEUTRAL_FIRE_BASE_DB_KEY = "userNeutral"

    LAST_UPDATED_FIRE_BASE_DB_KEY = "lastUpdated"

    while True:
        # Set ph
        ph = convertVoltsToPh(ANALOG.voltage)

        FIREBASE_DB.update({PH_FIRE_BASE_DB_KEY: ph}, firebaseUser['idToken'])

        FIREBASE_DB.update({LAST_UPDATED_FIRE_BASE_DB_KEY: datetime.datetime.now().strftime("%m/%d/%Y, %H:%M:%S")}, firebaseUser['idToken'])

        print(datetime.datetime.now(), " : pH =", ph)

        # Set thresholds
        threshold = FIREBASE_DB.child(THRESHOLD_FIRE_BASE_DB_KEY).get().val()

        print(datetime.datetime.now(), " : threshold = ", threshold)

        userPhNeutral = FIREBASE_DB.child(USER_NEUTRAL_FIRE_BASE_DB_KEY).get().val()

        print(datetime.datetime.now(), " : userPhNeutral = ", userPhNeutral)

        baseThreshold = calcBaseThreshold(threshold, userPhNeutral)

        print(datetime.datetime.now(), " : baseThreshold = ", baseThreshold)

        acidThreshold = calcAcidThreshold(threshold, userPhNeutral)

        print(datetime.datetime.now(), " : acidThreshold = ", acidThreshold)

        # Refresh login token because, by design, it expires every hour.
        if (datetime.datetime.now() - authLoginDate).total_seconds() >= AUTH_TIMEOUT_TRIGGER:
            try:
                firebaseUser = FIREBASE_AUTH.refresh(firebaseUser['refreshToken'])

            except Exception as e:
                print(e)

                # Loop buzzer because there was a connection error and I should reboot the PI
                while True:
                    DebuggingBuzzer(1)

            authLoginDate = datetime.datetime.now()

            print(datetime.datetime.now(), "User token refreshed")

        if ph >= baseThreshold:

            if phState != PhStates.BASE:
                PUSH_SERVICE.notify_topic_subscribers(topic_name=SUBSCRIBER_TOPIC, message_body=ACID_HIGH_BODY, message_title=BASE_HIGH_TITLE)

                print(datetime.datetime.now(), " : Sent notification")

                phState = PhStates.BASE

            print(datetime.datetime.now(), " : pH too high!")

        elif ph <= acidThreshold:

            if phState != PhStates.ACID:
                PUSH_SERVICE.notify_topic_subscribers(topic_name=SUBSCRIBER_TOPIC, message_body=BASE_HIGH_BODY, message_title=ACID_HIGH_TITLE)

                print(datetime.datetime.now(), " : Sent notification")

                phState = PhStates.ACID

            print(datetime.datetime.now(), " : pH too low!")

        else:

            if phState != PhStates.NEUTRAL:
                PUSH_SERVICE.notify_topic_subscribers(topic_name=SUBSCRIBER_TOPIC, message_body=PH_NEUTRAL_BODY, message_title=PH_NEUTRAL_TITLE)

                print(datetime.datetime.now(), " : Sent notification")

                phState = PhStates.NEUTRAL

            print(datetime.datetime.now(), " : pH stable")

        sleep(2)
